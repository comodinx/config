'use strict';

const config = require('./config');

describe('@comodinx/config', () => {
    describe('get', () => {
        it('should return correct value for "server.port"', done => {
            const result = config('server.port');

            expect(result).to.be.a('number').equal(8080);
            done();
        });

        it('should return correct value for "server.host"', done => {
            const result = config('server.host');

            expect(result).to.be.a('string').equal('0.0.0.0');
            done();
        });

        it('should return correct value for "my.other.anidate.config"', done => {
            const result = config('my.other.anidate.config');

            expect(result).to.be.a('string').equal('Hello testing World!');
            done();
        });

        it('should return undefined when key not exist', done => {
            const result = config('not.exists.config.key');

            expect(result).to.be.an('undefined');
            done();
        });

        it('should return default value when key not exist', done => {
            const result = config('not.exists.config.key', 1);

            expect(result).to.be.an('number').equal(1);
            done();
        });

        it('should return correct values when extend', done => {
            config.extend({
                my: {
                    other: {
                        anidate: {
                            config1: 'Hello testing World 1!',
                            config2: 'Hello testing World 2!'
                        }
                    }
                },
                other: {
                    config3: 'Hello testing World 3!'
                }
            });

            config.extend({
                my: {
                    other: {
                        anidate: {
                            config2: 'Hello testing World 2!'
                        }
                    }
                }
            });

            const result1 = config('my.other.anidate.config1');
            const result2 = config('my.other.anidate.config2');
            const result3 = config('other.config3');

            expect(result1).to.be.a('string').equal('Hello testing World 1!');
            expect(result2).to.be.a('string').equal('Hello testing World 2!');
            expect(result3).to.be.a('string').equal('Hello testing World 3!');
            done();
        });
    });
});
