'use strict';

const fs = require('fs');
const rd = require('require-directory');
const dotenv = require('dotenv');
const _ = require('./helpers');

// Initialize configuration with .env file.
dotenv.config();

let env = process.env.NODE_ENV || 'production';

// Check if need load extra environment configuration.
if (process.env.CONFIG_EXTRA_ENVIRONMENT || false) {
    const path = `.env.${env}`;

    if (fs.existsSync(path)) {
        Object.assign(process.env, dotenv.parse(fs.readFileSync(path)));
    }
    else {
        console.warn(`CONFIG_EXTRA_ENVIRONMENT is enabled, but the file ${path} not exists on root directory.`);
    }
}

const separator = process.env.CONFIG_SEPARATOR || '.';
const isPrivate = ['true', '1'].includes(process.env.CONFIG_PRIVATE || false);
const objectConfig = {};

env = process.env.NODE_ENV || 'production';

/**
 * Read config
 */
function config (key, defaultValue, options = {}) {
    options = options || {};

    let value = _get(objectConfig, `${env}${options.separator || separator}${key}`);

    if (value === undefined) {
        value = _get(objectConfig, key, defaultValue);
    }
    return value;
}

/**
 * Extend current config with other object.
 */
config.extend = function (source) {
    _.extend(objectConfig, source);
    return config;
};

/**
 * Require all files in a directory.
 */
config.require = function (/* module */ m, dirname, options) {
    return config.extend(rd(m, dirname, options));
};

/**
 * Define separator property
 */
Object.defineProperty(config, 'separator', {
    get: function () {
        return separator;
    }
});

/**
 * Get function
 */
function _get (object, pathString, defaultValue) {
    // Coerce pathString to a string (even it turns into "[object Object]").
    const parts = (pathString + '').split(separator);
    const length = parts.length;
    let i = 0;

    // In case object isn't a real object, set it to undefined.
    let value = object === Object(object) ? object : undefined;

    while (value != null && i < length) {
        value = value[parts[i++]];
    }

    /**
     * lodash.get() returns the resolved value if
     * 1. iteration happened (i > 0)
     * 2. iteration completed (i === length)
     * 3. the value at the path is found in the data structure (not undefined). Note that if the path is found but the
     *    value is null, then null is returned.
     * If any of those checks fails, return the defaultValue param, if provided.
     */
    return i && i === length && value !== undefined ? value : defaultValue;
};

if (!isPrivate) {
    /**
     * Define raw property, only if config is public
     */
    Object.defineProperty(config, 'raw', {
        get: function () {
            return objectConfig;
        }
    });
}

module.exports = config;
