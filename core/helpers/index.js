'use strict';

const arraySlice = Array.prototype.slice;

function isSpecificValue (value) {
    return value instanceof Buffer || value instanceof Date || value instanceof RegExp;
}

function clone (value) {
    if (value instanceof Buffer) {
        // eslint-disable-next-line node/no-deprecated-api
        const buffer = Buffer.alloc ? Buffer.alloc(value.length) : new Buffer(value.length);
        value.copy(buffer);
        return buffer;
    }
    else if (value instanceof Date) {
        return new Date(value.getTime());
    }
    else if (value instanceof RegExp) {
        return new RegExp(value);
    }
    throw new Error('Unexpected situation');
}

function cloneArray (array) {
    const cloned = [];

    array.forEach((item, index) => {
        if (typeof item === 'object' && item !== null) {
            if (Array.isArray(item)) {
                cloned[index] = cloneArray(item);
            }
            else if (isSpecificValue(item)) {
                cloned[index] = clone(item);
            }
            else {
                cloned[index] = extend({}, item);
            }
        }
        else {
            cloned[index] = item;
        }
    });
    return cloned;
}

function safeGetProperty (object, property) {
    return property === '__proto__' ? undefined : object[property];
}

/**
 * Extening object that entered in first argument.
 *
 * Returns extended object or false if have no target object or incorrect type.
 *
 * If you wish to clone source object (without modify it), just use empty new
 * object as first argument, like this:
 *   extend({}, object1 [, objectN]);
 */
function extend (/* object1, [object2], [objectN] */) {
    if (arguments.length < 1 || typeof arguments[0] !== 'object') {
        return false;
    }

    if (arguments.length < 2) {
        return arguments[0];
    }

    const target = arguments[0];

    // convert arguments to array and cut off target object
    const args = arraySlice.call(arguments, 1);

    let val;
    let src;

    args.forEach(object => {
        // skip argument if isn't an object, is null, or is an array
        if (typeof object !== 'object' || object === null || Array.isArray(object)) {
            return;
        }

        Object.keys(object).forEach(key => {
            src = safeGetProperty(target, key); // source value
            val = safeGetProperty(object, key); // new value

            // recursion prevention
            if (val === target) {
                return;
            }
            /**
             * if new value isn't object then just overwrite by new value
             * instead of extending.
             */
            else if (typeof val !== 'object' || val === null) {
                target[key] = val;
                return;
            }
            // just clone arrays (and recursive clone objects inside)
            else if (Array.isArray(val)) {
                target[key] = cloneArray(val);
                return;
            }
            // custom cloning and overwrite for specific objects
            else if (isSpecificValue(val)) {
                target[key] = clone(val);
                return;
            }
            // overwrite by new value if source isn't object or array
            else if (typeof src !== 'object' || src === null || Array.isArray(src)) {
                target[key] = extend({}, val);
                return;
            }

            // source value and new value is objects both, extending...
            target[key] = extend(src, val);
        });
    });

    return target;
};

module.exports = {
    extend
};
