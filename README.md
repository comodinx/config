# Config

@comodinx/config is a Node.js configuration helper.

## Index

* [Download & Install][install].
* [How is it used?][how_is_it_used].
* [Tests][tests].

## Download & Install

### NPM
```bash
    npm install @comodinx/config
```

### Source code
```bash
$ git clone https://gitlab.com/comodinx/config.git
$ cd config
$ npm install
```

## How is it used?

 + Static.
 + Extensible.
 + Configurable by environment.
 + Try a function to automatically require all files and folders recursively, keeping the naming structure.

### Configure

#### Environment options

| Environment variable     | Value         | Default value | Description                                                                                  |
|:-------------------------|:--------------|:--------------|:---------------------------------------------------------------------------------------------|
| CONFIG_SEPARATOR         | string        | `"."``        | Indicate config key separator.                                                               |
| CONFIG_PRIVATE           | boolean       | `false`       | Indicate if config object has raw property.                                                  |
| CONFIG_EXTRA_ENVIRONMENT | boolean       | `false`       | Indicates that in addition to loading the `.env` file, the `.env.{NODE_ENV}` will be loaded. |

#### Folder structure
```
 <My config folder name>
 |
 | -> index.js
 |    // Content: `module.exports = require('@comodinx/config').require(module, __dirname);`
 |
 | -> server.js
 |    // Example server file: `module.exports = { port: 8080 };`
 | -> my-config-file.js
 |    // Other example config file: `module.exports = { key: value, a: 'b', c: 1 };`
 | -> ...
 |
 | -> my-config-folder
 |     | -> types.js
 |          // Other example config file anidate: `module.exports = { 1: 'immediate', 2: 'periodic', 3: 'scheduled' };`
 |     | -> ...
 |
 | -> production
 |     | -> types.js
 |          // Override types for production environment.
 |     | -> ...
```

```js
const config = require('@comodinx/config');

// Simple usage
config('server.port'); // return: port value
config('my.not.exist.config'); // return: undefined
config('my.not.exist.config', 'my-default-value'); // return: 'my-default-value'

// Extend with object
config.extend({
  my: {
    config: {
      object: {
        hello: 'world!'
      }
    }
  }
});
config('my.config.object.hello'); // return: 'world!'
config('my.config.object'); // return: { hello: 'world!' }
config('my.config'); // return: { object: { hello: 'world!' } }

// Require all files on current folder
config.require(module, __dirname);
// Require all files on other folder
config.require(module, './my/other/folder');
// See more options on https://www.npmjs.com/package/require-directory#options
config.require(module, __dirname, {
  // Blacklist
  exclude: /dontinclude\.js$/,
  // Rename keys
  rename: name => name.toUpperCase()
});
```

#### Example when `CONFIG_EXTRA_ENVIRONMENT` is enabled

Example project folder structure
```
 <root>
 |
 | -> config
 |    // Content: all configuration files
 |
 | -> index.js
 |    // Content: project main source code
```

`config` folder
```
 config
 |
 | -> index.js
 |    // Content: `module.exports = require('@comodinx/config').require(module, __dirname);`
 |
 | -> server.js
 |    // Example server file: `module.exports = { port: process.env.SERVER_PORT, host: process.env.SERVER_HOST };`
```

`.env` file (on project root)
```
NODE_ENV=local
SERVER_PORT=8001
SERVER_HOST=localhost
CONFIG_EXTRA_ENVIRONMENT=true
```

`.env.local` file (on project root)
```
NODE_ENV=local
SERVER_PORT=8080
```

`index.js` main source code (on project root)
```js
const config = require('./config');

// Simple usage
config('server.port'); // return: 8080
config('server.host'); // return: localhost
```


## Tests

In order to see more concrete examples, **I INVITE YOU TO LOOK AT THE TESTS :)**

### Run the unit tests
```sh
npm test
```

<!-- deep links -->
[install]: #download--install
[how_is_it_used]: #how-is-it-used
[tests]: #tests
